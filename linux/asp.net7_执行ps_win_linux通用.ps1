Set-Location ${PSScriptRoot}
Add-Type -Path "${PSScriptRoot}\Microsoft.AspNetCore.dll"

class PowerShellDelegate
{
    [string] $_script

    PowerShellDelegate([string] $script)
    {
        $this._script = $script
    }

    [System.Threading.Tasks.Task] Invoke($context)
    {
        $powershell = [PowerShell]::Create()
        try
        {
            $powershell.AddScript($this._script).AddParameter("HttpContext", $context)
            $powerShell.Invoke()
        }
        finally
        {
            $powerShell.Dispose()
        }

        return [System.Threading.Tasks.Task]::CompletedTask
    }
}

$script = @'
param($HttpContext)
$a = get-date -format F
$b = [System.Text.Encoding]::GetEncoding(936).GetBytes($a)
$HttpContext.Response.Body.WriteAsync($b, 0, $b.Length).AsTask().Wait()
'@

$Delegate = [powerShellDelegate]::new($script)

$App = [Microsoft.AspNetCore.Builder.WebApplication]::Create()
[Void][Microsoft.AspNetCore.Builder.EndpointRoutingApplicationBuilderExtensions]::UseRouting($App)
[Void][Microsoft.AspNetCore.Builder.EndpointRoutingApplicationBuilderExtensions]::UseEndpoints($App, { [Microsoft.AspNetCore.Builder.EndpointRouteBuilderExtensions]::MapGet($args[0],"/", $Delegate.Invoke) })
$App.Run()
