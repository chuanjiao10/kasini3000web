﻿#
Set-Location ${PSScriptRoot}

if ( ($IsWindows -eq $True) -or ($PSVersionTable.psversion.major -lt 6) ) #win
{
	Set-Location '../win'
}

if ($IsLinux -eq $True)
{
	Set-Location '../linux'
}

Add-Type -Path ".\Microsoft.AspNetCore.dll"
Set-Location ${PSScriptRoot}


class PowerShellDelegate
{
	[string] $_script

	[string] $_pagefile

	PowerShellDelegate([string]$script,[string]$pagefile1)
	{
		$this._script = $script
		$this._pagefile = "${PSScriptRoot}/${pagefile1}"
	}

	[System.Threading.Tasks.Task] Invoke($context)
	{
		$powershell = [PowerShell]::Create()
		try
		{
			$powershell.AddScript($this._script).
			AddParameter("HttpContext", $context).
			AddParameter("pagefile2", $this._pagefile)
			$powerShell.Invoke()
		}
		finally
		{
			$powerShell.Dispose()
		}

		return [System.Threading.Tasks.Task]::CompletedTask
	}
}

#问：这个脚本谁写的？有问题找谁技术支持？
#答：QQ群号=183173532
#名称=powershell交流群
#华之夏，脚之巅，有我ps1片天！
#专门教学win，linux通用的ps1脚本。不想学也可以，入群用红包求写脚本。

#---------------------------------------------------------------------
$主页代码 = {
	param($HttpContext,$pagefile2)
	$pindex = Get-Content -Path $pagefile2 -Raw
	$a = [System.Text.Encoding]::UTF8.GetBytes($pindex)
	$HttpContext.Response.Body.WriteAsync($a, 0, $a.Length).AsTask().Wait()
}
$主页委托 = [powerShellDelegate]::new($主页代码,'index.html')

#---------------------------------------------------------------------
$获取时间代码 = {
	param($HttpContext)
	$d = Get-Date -Format F
	$a = [System.Text.Encoding]::GetEncoding(936).GetBytes($d)
	$HttpContext.Response.Body.WriteAsync($a, 0, $a.Length).AsTask().Wait()
}
$获取时间委托 = [powerShellDelegate]::new($获取时间代码,'')

#---------------------------------------------------------------------
# $env:ASPNETCORE_URLS = 'http://0.0.0.0:80'
$App = [Microsoft.AspNetCore.Builder.WebApplication]::Create()
[Void][Microsoft.AspNetCore.Builder.EndpointRoutingApplicationBuilderExtensions]::UseRouting($App)
[Void][Microsoft.AspNetCore.Builder.EndpointRoutingApplicationBuilderExtensions]::UseEndpoints($App, { [Microsoft.AspNetCore.Builder.EndpointRouteBuilderExtensions]::MapGet($args[0],"/", $主页委托.Invoke) })
[Void][Microsoft.AspNetCore.Builder.EndpointRoutingApplicationBuilderExtensions]::UseEndpoints($App, { [Microsoft.AspNetCore.Builder.EndpointRouteBuilderExtensions]::MapPost($args[0],"/getdate/", $获取时间委托.Invoke) })
$App.Run()




#---------------------------------------------------------------------
