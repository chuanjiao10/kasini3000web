# win-linux 批量运维宝 web控制台

项目还未完成，目前暂时无法使用。只能看到主页。

![主页](主页.jpg)


### 简介：

在 win，linux下，用powershell开发动态web网页的项目。

powershell 调用 跨平台的 asp.net 库实现的。

因为win， linux目录下的dll文件，是.net7的，所以目前只支持 win，linux版powershell v7.3调用。

因为默认监听在  http://127.0.0.1:5000     ，不需要登录。

### 用法：

1 在win，linux中，启动powershellv7.3.x。

2 cd 项目目录；./start.ps1

3 用浏览器访问： http://127.0.0.1:5000

------

# example：

example 目录下是powershell动态网页例子。相当于hello world。

### 用法：

1 在win，linux中，启动powershellv7.3.x。

2 cd 项目目录/example ；./example.ps1

3 用浏览器访问： http://127.0.0.1:5000

![example](ps1web.gif)

上图为gif图像，如果未能显示动画，请另存为本地文件后，再打开。

------

### 问：如何更改监听端口？

答：
```powershell
$env:ASPNETCORE_URLS = 'http://0.0.0.0:80'
```

把上述代码，放在下述代码前面：
```
$App = [Microsoft.AspNetCore.Builder.WebApplication]::Create()
```

* 1024以下端口，需要管理员权限。
* 首次运行，win会弹出防火墙。
* linux也需要开启，防火墙上的对应端口。



------


# MIT License
